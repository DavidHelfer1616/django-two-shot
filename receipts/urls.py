from django.urls import path
from receipts.views import create_account, create_category, create_receipt, get_all_accounts, get_all_categories, get_all_receipts


urlpatterns = [
    path("", get_all_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", get_all_categories, name="category_list"),
    path("accounts/", get_all_accounts, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account")
]
