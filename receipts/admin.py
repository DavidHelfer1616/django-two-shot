from django.contrib import admin

from receipts.models import Account, ExpenseCategory, Receipt

@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "owner",
    ]

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "number",
        "owner",
    ]

@admin.register(Receipt)
class ReciptAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "vendor",
        "total",
        "tax",
        "purchaser",
        "category",
        "account",
    ]
